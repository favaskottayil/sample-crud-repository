package com.example.demo;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.controller.EmplController;
import com.example.demo.entity.EmpEntity;
import com.example.demo.repository.EmpRepo;



@SpringBootApplication
//@EnableAuthorizationServer
public class SampleApplication implements CommandLineRunner  {
    
	@Autowired
	EmpRepo empRepo;

	public static void main(String[] args) {
		
		SpringApplication.run(SampleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
		List<EmpEntity> empList=Arrays.asList( new EmpEntity("favas", "9745"),new EmpEntity("adasf", "541515"), new EmpEntity("Testsyam", "654646"));
		
		empList.forEach(empRepo::save);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		// TODO Auto-generated method stub
//		
//		Collection<Todo> todos = Arrays.asList(new Todo("Learn Spring", "Yes"), new Todo("Learn Driving", "No"), new Todo("Go for a Walk", "No"), new Todo("Cook Dinner", "Yes"));
//		todos.forEach(todoRepo::save);
//	}
	

}
