//package com.example.demo.controller;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//
//import com.example.demo.entity.Todo;
//import com.example.demo.repository.TodoRepo;
//
//
//@Controller
//public class TodoController {
//
//	@Autowired
//	TodoRepo todoRepo;
//	@GetMapping
//	public String index(){
//		return "index";
//	}
//	@GetMapping("/todos")
//	public String todos(Model model) {
//		List<Todo> todoList = new ArrayList<Todo>();
//		   todoRepo.findAll().forEach(todo->todoList.add(todo));
// 		model.addAttribute("todos", todoRepo.findAll());
//		return "todos";
//	}
//}
