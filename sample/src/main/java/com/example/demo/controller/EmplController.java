package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.EmpEntity;
import com.example.demo.repository.EmpRepo;

@RestController
@RequestMapping("emp/")
public class EmplController {
	
	@Autowired
    EmpRepo empRepo;
	
	@PostMapping("register")
	public ResponseEntity<Void>registerEmp(@RequestBody EmpEntity employee){
		empRepo.save(employee);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	@GetMapping("{id}")
	public ResponseEntity<Optional<EmpEntity>>getEmp(@PathVariable("id") Integer id){
		Optional<EmpEntity> emp=empRepo.findById(id);
		return new ResponseEntity<Optional<EmpEntity>>(emp,HttpStatus.OK);
	}
	@GetMapping("viewAll")
	public ResponseEntity<List<EmpEntity>>viewAll(){
		System.out.println("push CHeck");
		 List<EmpEntity> empList = new ArrayList<EmpEntity>();
		 empRepo.findAll().forEach(e->empList.add(e));
		return new ResponseEntity<List<EmpEntity>>(empList,HttpStatus.OK) ;
	}
	@DeleteMapping("delete/{id}")
	public ResponseEntity<Void> deleteEmp(@PathVariable("id")Integer id ){
		empRepo.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	@PutMapping("edit")
	public ResponseEntity<Entity>editEmp(@RequestBody EmpEntity emp){
		empRepo.save(emp);
		return new ResponseEntity<Entity>(HttpStatus.OK);
	}

}
