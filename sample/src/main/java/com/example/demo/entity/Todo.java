//package com.example.demo.entity;
//
//import java.io.Serializable;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//
//
//@Entity
//public class Todo implements Serializable {
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	private long id;
//	private String todoItem;
//	private String completed;
//	
//	 public Todo(String todoItem, String completed) {
//		super();
//		this.todoItem = todoItem;
//		this.completed = completed;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getTodoItem() {
//		return todoItem;
//	}
//
//	public void setTodoItem(String todoItem) {
//		this.todoItem = todoItem;
//	}
//
//	public String getCompleted() {
//		return completed;
//	}
//
//	public void setCompleted(String completed) {
//		this.completed = completed;
//	}
//	
//	
//	
//	
//}
