package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.entity.EmpEntity;



public interface EmpRepo extends CrudRepository<EmpEntity, Integer> {
	

}
